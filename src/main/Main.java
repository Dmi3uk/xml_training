package main;

import java.io.File;
import java.io.IOException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class Main {

	public static void main(String[] args) {
		try {
			File inputFile = new File("src/Students.xml");
	        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
	        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
	        Document doc = dBuilder.parse(inputFile);
	        Element rootElement = doc.getDocumentElement();
	        String groupName = rootElement.getAttribute("name");
	        System.out.println(groupName);
	        NodeList studentNodes = rootElement.getElementsByTagName("STUDENT");
	        for (int i = 0; i < studentNodes.getLength(); i++) {
	        	Element studentElement = (Element)studentNodes.item(i);
	        	Student student = new Student(studentElement.getAttribute("name"));
	        	Element marksElement = 
	        			(Element)studentElement.getElementsByTagName("MARKS").item(0);
	        	NodeList marksNodes = marksElement.getChildNodes();
	        	for (int j = 0; j < marksNodes.getLength(); j++) {
	        		if (marksNodes.item(j).getNodeType() == Node.ELEMENT_NODE) {
	        			String mark = marksNodes.item(j).getTextContent();
	        			student.marks.add(Integer.parseInt(mark));
	        		}
	        	}
	        	Element visitElement = 
	        			(Element)studentElement.getElementsByTagName("VISITS").item(0);
	        	NodeList visitsNodes = visitElement.getChildNodes();
	        	for (int j = 0; j < visitsNodes.getLength(); j++) {
	        		if (visitsNodes.item(j).getNodeType() == Node.ELEMENT_NODE) {
	        			String visit = visitsNodes.item(j).getTextContent();
	        			student.visits.add(visit);
	        		}
	        	}
	        	System.out.println(student);
	        }
		} catch (ParserConfigurationException | SAXException | IOException e) {
			System.out.println("import error: " + e.getMessage());
		}

	}

}
