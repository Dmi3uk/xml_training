package main;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Student {
	public String name;
	public List<Integer> marks;
	public List<String> visits;
	
	public Student(String name) {
		super();
		this.name = name;
		this.marks = new ArrayList<>();
		this.visits = new ArrayList<>();
	}

	@Override
	public String toString() {
		return "Student [name=" + name + ", marks=" + marks + ", visits=" + visits + "]";
	}
	
}
